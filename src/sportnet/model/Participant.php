<?php
	namespace sportnet\model;
	use \sportnet\utils\ConnectionFactory as ConnectionFactory;
	use \sportnet\model\Run as Run ;

	class Participant extends AbstractModel {

		protected $numParticipant,$firstName,$lastName,$birthDate,$email;
		protected $db;

		public function __construct(){
			$this->db = ConnectionFactory::makeConnection();

		}


		public function getRun(){
			$listRuns = [];
			$requete = "SELECT * from participation JOIN run ON participation.idRun = run.id where idParticipant = :idPart";

			$requete_prep = $this->db->prepare( $requete );

			$requete_prep->bindParam( ':idPart', $this->numParticipant, \PDO::PARAM_INT ) ;

			if ($requete_prep->execute()){
			       while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$Run = new Run();
					$Run->id = $ligne->id;
					$Run->label = $ligne->label;
					$Run->startTime = $ligne->startTime;
					$Run->startDate = $ligne->startTime;
					$Run->description = $ligne->description;
					$Run->price = $ligne->price;
					$Run->idEvent = $ligne->idEvent;

					$listRuns[] = $Run;

				}

			}

			return $listRuns;
		}

		public static function findById($id){
			$db = ConnectionFactory::makeConnection();
			$requete = "SELECT * FROM Participant where numParticipant = :numParticipant ";

			$requete_prep = $db->prepare( $requete );

			$requete_prep->bindParam( ':numParticipant', $id, \PDO::PARAM_INT ) ;

			if ($requete_prep->execute()){
				while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
						$p = new Participant ;
						$p->numParticipant = $ligne->numParticipant;
						$p->lastName = $ligne->lastName;
						$p->firstName= $ligne->firstName;
						$p->birthDate = $ligne->birthDate;
						$p->email = $ligne->email;
						return $p;
					}
				
			} else{
					return false;
			}
		}

		public function insert(){

			$work = true ;

			$requete = "INSERT INTO Participant (firstName,lastName,email,birthDate) VALUES ( :firstName, :lastName, :email, :birthDate)";

			$requete_prep = $this->db->prepare( $requete );

			$requete_prep->bindParam( ':firstName', $this->firstName, \PDO::PARAM_STR ) ;
			$requete_prep->bindParam( ':lastName', $this->lastName, \PDO::PARAM_STR ) ;
			$requete_prep->bindParam( ':birthDate', $this->birthDate, \PDO::PARAM_STR ) ;
			$requete_prep->bindParam( ':email', $this->email, \PDO::PARAM_STR ) ;

			if($requete_prep->execute()){
				$this->numParticipant = $this->db->lastInsertId();
				$work = $this->numParticipant;

			} else {
				$work = false ;
			}

			return $work;

		}




	}
