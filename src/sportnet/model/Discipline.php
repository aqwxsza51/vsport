<?php
namespace sportnet\model;

use \sportnet\utils\ConnectionFactory as ConnectionFactory;

class Discipline extends AbstractModel {

	protected $id, $label;
	protected $db;

	public function __construct(){
		$this->db = ConnectionFactory::makeConnection();
	}

	public static function findAll(){
		$db = ConnectionFactory::makeConnection();
		$list = [];
		$requete = "SELECT * FROM discipline";
		$resultat = $db->query($requete);
		if ($resultat){
      while ( $ligne =  $resultat->fetch( \PDO::FETCH_OBJ ) ){
				$d = new Discipline();
				$d->id = $ligne->id;
				$d->label = $ligne->label;
				$list[] = $d;
			}
			return $list;

		}
		else{
    			return false;
		}
	}
}
