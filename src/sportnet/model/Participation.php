<?php
	namespace sportnet\model;
	use \sportnet\utils\ConnectionFactory as ConnectionFactory;

	class Participation extends AbstractModel {
		protected $idParticipant,$idRun,$time,$rank,$bib ;
		protected $db;


		public function __construct(){
			$this->db = ConnectionFactory::makeConnection();

		}

		public static function findByParticipant($idParticipant){
			$list = [];
			$db = ConnectionFactory::makeConnection();
			$participation = false;
			$requete = "SELECT * FROM participation where idParticipant = :idParticipant";

			$requete_prep = $db->prepare( $requete );

			$requete_prep->bindParam( ':idParticipant', $idParticipant, \PDO::PARAM_INT ) ;

			if ($requete_prep->execute()){
				while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$participation = new Participation();
					$participation->idParticipant = $ligne->idParticipant;
					$participation->idRun = $ligne->idRun;
					$participation->time = $ligne->time;
					$participation->rank = $ligne->rank;
					$participation->bib = $ligne->bib;
					$list[] = $participation;
				}
			}
			return $list;
		}



		public static function findByParticipantRun($idParticipant,$Run){

			$db = ConnectionFactory::makeConnection();
			$participation = false;
			$requete = "SELECT * FROM participation where idParticipant = :idParticipant AND idRun = :idRun";

			$requete_prep = $db->prepare( $requete );

			$requete_prep->bindParam( ':idParticipant', $idParticipant, \PDO::PARAM_INT ) ;
			$requete_prep->bindParam( ':idRun', $idRun, \PDO::PARAM_INT ) ;

			if ($requete_prep->execute()){
				while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$participation = new Participation();
					$participation->idParticipant = $ligne->idParticipant;
					$participation->idRun = $ligne->idRun;
					$participation->time = $ligne->time;
					$participation->rank = $ligne->rank;
					$participation->bib = $ligne->bib;
				}
			}
			return $participation;
		}


		public function getRegistrationForAllRun() {

			$list = [] ;
			$db = ConnectionFactory::makeConnection();
			$participation = false;
			$requete = "SELECT * FROM Run JOIN Participation ON id = idRun JOIN Participant ON idParticipant = numParticipant where idParticipant = :idParticipant";

			$requete_prep = $db->prepare( $requete );

			$requete_prep->bindParam(':idParticipant', $this->idParticipant, \PDO::PARAM_INT ) ;

			if ($requete_prep->execute()){
				while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$list[] = array($ligne->label,$ligne->firstName,$ligne->lastName,$ligne->bib);
				}
			}

			return $list;

		}


		public function insert(){
			$work = true ;
			$bib = "";
			$requete = "SELECT COUNT(*) as nbRegi from Participation where idRun = :idRun";

			$requete_prep = $this->db->prepare($requete);

			$requete_prep->bindParam( ':idRun', $this->idRun, \PDO::PARAM_INT ) ;

			if ($requete_prep->execute()){
			     $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ;
			}

			$bib = $ligne->nbRegi ;
			$bib = $bib + 1 ;

			$requete = "INSERT INTO Participation (idParticipant,idRun,bib) VALUES ( :idParticipant, :idRun, :bib)";

			$requete_prep = $this->db->prepare( $requete );

			$requete_prep->bindParam( ':idParticipant', $this->idParticipant, \PDO::PARAM_INT ) ;
			$requete_prep->bindParam( ':idRun', $this->idRun, \PDO::PARAM_INT ) ;
			$requete_prep->bindParam( ':bib', $bib, \PDO::PARAM_INT ) ;

			if($requete_prep->execute()){

				$work = true;

			} else {
				$work = false ;
			}

			return $work;

		}

		public function updateRank($idRun, $idParticipant, $time, $rank){
			$requete = "UPDATE participation SET time = :time, rank = :rank WHERE idRun = :idRun and idParticipant = :idParticipant";

			$requete_prep = $this->db->prepare($requete);

			$requete_prep->bindParam(':time', $time, \PDO::PARAM_STR);
			$requete_prep->bindParam(':rank', $rank, \PDO::PARAM_INT);
			$requete_prep->bindParam(':idRun', $idRun, \PDO::PARAM_INT);
			$requete_prep->bindParam(':idParticipant', $idParticipant, \PDO::PARAM_INT);

			if($requete_prep->execute()){
				return true;
			}
			return false;
		}

		public function getRegistrationForAllParticipant() {
			$list = [] ;
			$requete = "SELECT * FROM Run JOIN Participation ON id = idRun JOIN Participant ON idParticipant = numParticipant where idRun = :idRun";
			$requete_prep = $this->db->prepare( $requete );
			$requete_prep->bindParam(':idRun', $this->idRun, \PDO::PARAM_INT ) ;
			if ($requete_prep->execute()){
				while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$list[] = array($ligne->idParticipant,$ligne->firstName,$ligne->lastName,$ligne->bib);
				}
				return $list;
			}
			else {
				return false;
			}
		}

}
