<?php
	namespace sportnet\model;
	use \sportnet\utils\ConnectionFactory as ConnectionFactory;


	class Organiser extends AbstractModel {



		protected $id,$pass,$email,$name;
		protected $db;

		public function __construct(){
			$this->db = ConnectionFactory::makeConnection();

		}


		public function insert(){

			$work = true ;
			$requete = "INSERT INTO organiser (pass,email,name) VALUES ( :pass, :email, :name)";

			$requete_prep = $this->db->prepare( $requete );

			$requete_prep->bindParam( ':pass', $this->pass, \PDO::PARAM_STR ) ;
			$requete_prep->bindParam( ':email', $this->email, \PDO::PARAM_STR ) ;
			$requete_prep->bindParam( ':name', $this->name, \PDO::PARAM_STR ) ;


			if($requete_prep->execute()){
				$this->id = $this->db->lastInsertId();
				$work = "Organisateur ajouté avec succès";
			} else {
				$work = false ;
			}

			return $work ;

		}


		public function getEvents(){
			$listEvents = [];
			$requete = "SELECT * from event where idOrga = :id";

			$requete_prep = $this->db->prepare( $requete );

			$requete_prep->bindParam( ':id', $this->id, \PDO::PARAM_INT ) ;

			if ($requete_prep->execute()){
			       while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$event = new Event();
					$event->id = $ligne->id;
					$event->label = $ligne->label;
					$event->startDate = $ligne->startDate;
					$event->endDate = $ligne->endDate;
					$event->description = $ligne->description;
					$event->status = $ligne->status;
					$event->website = $ligne->website;
					$event->idDiscipline = $ligne->idDiscipline;
					$event->photo = $ligne->photo;
					$event->place = $ligne->place;
					$listEvents[] = $event;

				}

			}

			return $listEvents;

		}



		public static function findByLogin($email){

			$db = ConnectionFactory::makeConnection();
			$orga = false;
			$requete = "SELECT name,email,pass,id FROM organiser where email = :email";

			$requete_prep = $db->prepare( $requete );

			$requete_prep->bindParam( ':email', $email, \PDO::PARAM_STR ) ;

			if ($requete_prep->execute()){
				while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$orga = new Organiser();
					$orga->email = $ligne->email;
					$orga->name = $ligne->name;
					$orga->pass = $ligne->pass;
					$orga->id = $ligne->id;
				}
			}
			return $orga;
		}




	}
