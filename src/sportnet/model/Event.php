<?php
namespace sportnet\model;

use \sportnet\utils\ConnectionFactory as ConnectionFactory;

class Event extends AbstractModel {

	protected $id, $label, $startDate, $endDate, $description, $status, $website, $idDiscipline, $idOrga, $place, $photo;
	protected $db;

	public function __construct(){
		$this->db = ConnectionFactory::makeConnection();
	}

	public function updateStatus(){

		$requete = "UPDATE event SET status = :status where id = :id";
		$requete_prep = $this->db->prepare( $requete );

		$requete_prep->bindParam( ':status', $this->status, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':id', $this->id, \PDO::PARAM_INT ) ;

		if($requete_prep->execute())
			return true;
		return false;
	}


	public function insert(){
		$requete = "INSERT INTO event VALUES (NULL, :label, :startDate, :endDate, :description, :status, :website, :idDiscipline, :idOrga, :photo, :place)";

		$requete_prep = $this->db->prepare( $requete );

		$requete_prep->bindParam( ':label', $this->label, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':startDate', $this->startDate, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':endDate', $this->endDate, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':description', $this->description, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':status', $this->status, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':website', $this->website, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':idDiscipline', $this->idDiscipline, \PDO::PARAM_INT ) ;
		$requete_prep->bindParam( ':idOrga', $this->idOrga, \PDO::PARAM_INT ) ;
		$requete_prep->bindParam( ':photo', $this->photo, \PDO::PARAM_STR) ;
		$requete_prep->bindParam( ':place', $this->place, \PDO::PARAM_STR) ;

		if($requete_prep->execute()){

			$this->id = $this->db->lastInsertId();
			return $this->id;
		}

		return false;

	}

	public function save(){
	if (isset($this->id))
		$this->update();
	else
		$this->insert();
}

	public static function findAll(){
		$db = ConnectionFactory::makeConnection();
		$list = [];
		$requete = "SELECT * FROM event";
		$resultat = $db->query($requete);
		if ($resultat){
       while ( $ligne =  $resultat->fetch( \PDO::FETCH_OBJ ) ){
             		/* ligne est un objet anonyme */

				// echo "$ligne->id, $ligne->title, $ligne->text<br>";
				$e = new Event();
				$e->id = $ligne->id;
				$e->label = $ligne->label;
				$e->startDate = $ligne->startDate;
				$e->endDate = $ligne->endDate;
				$e->description = $ligne->description;
				$e->status = $ligne->status;
				$e->website = $ligne->website;
				$e->idDiscipline = $ligne->idDiscipline;
				$e->idOrga = $ligne->idOrga;
				$e->photo = $ligne->photo;
				$e->place = $ligne->place;
				$list[] = $e;
			}
			return $list;

		}
		else{
    			return false;
		}
	}

	public static function findById($id){
		$db = ConnectionFactory::makeConnection();

		$requete = "SELECT * FROM event where id = :id";

		$requete_prep = $db->prepare( $requete );

		$requete_prep->bindParam( ':id', $id, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
		  while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
						$e = new Event();
		 				$e->id = $ligne->id;
		 				$e->label = $ligne->label;
		 				$e->startDate = $ligne->startDate;
		 				$e->endDate = $ligne->endDate;
		 				$e->description = $ligne->description;
		 				$e->status = $ligne->status;
		 				$e->website = $ligne->website;
		 				$e->idDiscipline = $ligne->idDiscipline;
						$e->idOrga = $ligne->idOrga;
		 				$e->photo = $ligne->photo;
		 				$e->place = $ligne->place;
			}
			return $e;
		}
		else{
		    return false;
		}
	}

	public function getDiscipline(){
		$requete = "SELECT * FROM discipline where id = :id";

		$requete_prep = $this->db->prepare( $requete );

		$requete_prep->bindParam( ':id', $this->idDiscipline, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
		  	while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$d = new Discipline();
					$d->id = $ligne->id;
					$d->label = $ligne->label;
				}
			return $d;
		}
		else{
		    return false;
		}
	}

	public function getOrganiser(){
		$requete = "SELECT * FROM organiser where id = :id";

		$requete_prep = $this->db->prepare( $requete );

		$requete_prep->bindParam( ':id', $this->idOrga, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
				while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$o = new Organiser();
					$o->id = $ligne->id;
					$o->pass = $ligne->pass;
					$o->email = $ligne->email;
					$o->name = $ligne->name;
				}
			return $o;
		}
		else{
				return false;
		}
	}

	public function getRun(){
		$requete = "SELECT * FROM run where idEvent = :id";

		$requete_prep = $this->db->prepare( $requete );

		$requete_prep->bindParam( ':id', $this->id, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
			while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
					$r = new Run();
					$r->id = $ligne->id;
					$r->label = $ligne->label;
					$r->startTime = $ligne->startTime;
					$r->startDate = $ligne->startDate;
					$r->description = $ligne->description;
					$r->price = $ligne->price;
					$list[] = $r;
				}
			return $list;
		}
		else{
				return false;
		}
	}

  public static function findBecoming(){
		$db = ConnectionFactory::makeConnection();
		$list = [];
		$requete = "SELECT * FROM event where endDate > CURDATE() AND status != 'Résultats publiés' AND status != 'Événement créé'";
		$resultat = $db->query($requete);
		if ($resultat){
						while ( $ligne =  $resultat->fetch( \PDO::FETCH_OBJ ) ){
								/* ligne est un objet anonyme */

				// echo "$ligne->id, $ligne->title, $ligne->text<br>";
				$e = new Event();
				$e->id = $ligne->id;
				$e->label = $ligne->label;
				$e->startDate = $ligne->startDate;
				$e->endDate = $ligne->endDate;
				$e->description = $ligne->description;
				$e->status = $ligne->status;
				$e->website = $ligne->website;
				$e->idDiscipline = $ligne->idDiscipline;
				$e->idOrga = $ligne->idOrga;
				$e->photo = $ligne->photo;
				$e->place = $ligne->place;
				$list[] = $e;
			}
			return $list;

		}
		else{
					return false;
		}
	}

	public function countById(){
		$db = ConnectionFactory::makeConnection();

		$requete = "SELECT count(*) as count FROM event JOIN run on event.id = run.idEvent JOIN participation ON participation.idRun = run.id WHERE event.id = :id";

		$requete_prep = $db->prepare( $requete );

		$requete_prep->bindParam( ':id', $this->id, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
			while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
				$c = $ligne->count;
			}
			return $c;
		}
		else{
				return false;
		}
	}

	  public static function findwithRank(){
		$db = ConnectionFactory::makeConnection();
		$list = [];
		$requete = "SELECT * FROM event where status = 'Résultats publiés' ";
		$resultat = $db->query($requete);
		if ($resultat){
				while ( $ligne =  $resultat->fetch( \PDO::FETCH_OBJ ) ){
								/* ligne est un objet anonyme */

				// echo "$ligne->id, $ligne->title, $ligne->text<br>";
				$e = new Event();
				$e->id = $ligne->id;
				$e->label = $ligne->label;
				$e->startDate = $ligne->startDate;
				$e->endDate = $ligne->endDate;
				$e->description = $ligne->description;
				$e->status = $ligne->status;
				$e->website = $ligne->website;
				$e->idDiscipline = $ligne->idDiscipline;
				$e->idOrga = $ligne->idOrga;
				$e->photo = $ligne->photo;
				$e->place = $ligne->place;
				$list[] = $e;
			}
			return $list;

		}
		else{
					return false;

		}
	}

}
