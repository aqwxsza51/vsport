<?php
namespace sportnet\model;

use \sportnet\utils\ConnectionFactory as ConnectionFactory;
use \sportnet\model\Participation as Participation ;

class Run extends AbstractModel {

	protected $id, $label, $startTime, $startDate, $description, $price, $idEvent;
	protected $db;

	public function __construct(){
		$this->db = ConnectionFactory::makeConnection();
	}

	public function insert(){

		$requete = "INSERT INTO run VALUES (NULL, :label, :startTime, :startDate, :description, :price, :idEvent)";

		$requete_prep = $this->db->prepare( $requete );

		$requete_prep->bindParam( ':label', $this->label, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':startTime', $this->startTime, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':startDate', $this->startDate, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':description', $this->description, \PDO::PARAM_STR ) ;
		$requete_prep->bindParam( ':price', $this->price, \PDO::PARAM_STR) ;
		$requete_prep->bindParam( ':idEvent', $this->idEvent, \PDO::PARAM_INT) ;

		if($requete_prep->execute()){
			$this->id = $this->db->lastInsertId();
			return $this->id;
		}
		return false;

	}


	public static function findAll(){
		$db = ConnectionFactory::makeConnection();
		$list = [];
		$requete = "SELECT * FROM run";
		$resultat = $db->query($requete);
		if ($resultat){
      while ( $ligne =  $resultat->fetch( \PDO::FETCH_OBJ ) ){
				$r = new Run();
				$r->id = $ligne->id;
				$r->label = $ligne->label;
				$r->startTime = $ligne->startTime;
				$r->startDate = $ligne->startDate;
				$r->description = $ligne->description;
				$r->price = $ligne->price;
				$r->idEvent = $ligne->idEvent;
				$list[] = $r;
			}
			return $list;

		}
		else{
    			return false;
		}
	}

	public static function findById($id){
		$db = ConnectionFactory::makeConnection();

		$requete = "SELECT * FROM run where id = :id";

		$requete_prep = $db->prepare( $requete );

		$requete_prep->bindParam( ':id', $id, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
		  while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
				$r = new Run();
				$r->id = $ligne->id;
				$r->label = $ligne->label;
				$r->startTime = $ligne->startTime;
				$r->startDate = $ligne->startDate;
				$r->description = $ligne->description;
				$r->price = $ligne->price;
				$r->idEvent = $ligne->idEvent;
			}
			return $r;
		}
		else{
		    return false;
		}
	}

	public function getParticipation(){
		$list = [];
		$requete = "SELECT * FROM participation where idRun = :idRun order by rank";

		$requete_prep = $this->db->prepare( $requete );

		$requete_prep->bindParam( ':idRun', $this->id, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
		       while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
				$p = new Participation();
				$p->idParticipant = $ligne->idParticipant;
				$p->idRun = $ligne->idRun;
				$p->time = $ligne->time;
				$p->bib= $ligne->bib;
				$p->rank= $ligne->rank;
				$list[] = $p;
			}
			return $list;

		}
		else{
    			return false;
		}

	}

	public function countById(){
		$db = ConnectionFactory::makeConnection();

		$requete = "SELECT count(*) as count FROM participation where idRun = :id";

		$requete_prep = $db->prepare( $requete );

		$requete_prep->bindParam( ':id', $this->id, \PDO::PARAM_INT ) ;

		if ($requete_prep->execute()){
			while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
				$c = $ligne->count;
			}
			return $c;
		}
		else{
				return false;
		}
	}


	public static function findByLogin($login){
		$db = ConnectionFactory::makeConnection();

		$requete = "SELECT * FROM user where login = :login";

		$requete_prep = $db->prepare( $requete );

		$requete_prep->bindParam( ':login', $login, \PDO::PARAM_STR ) ;

		if ($requete_prep->execute()){
		  while ( $ligne =  $requete_prep->fetch( \PDO::FETCH_OBJ ) ){
				$u = new Run();
				$u->id = $ligne->id;
				$u->login = $ligne->login;
				$u->pass = $ligne->pass;
				$u->level = $ligne->level;
			}
			return $u;
		}
		return false;
	}
}
