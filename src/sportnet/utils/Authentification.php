<?php
namespace sportnet\utils;

use \sportnet\model\Organiser as Organiser;

class Authentification extends AbstractAuthentification {

	public function __construct(){
		if (isset($_SESSION['user_login'])){
			$this->user_login = $_SESSION['user_login'];
			$this->access_level = $_SESSION['access_level'];
			$this->logged_in = true;
		}
		else {
			$this->user_login = null;
			$this->access_level = ACCESS_LEVEL_NONE;
			$this->logged_in = false;
		}
	}

	public function login($login, $pass){
		$o = Organiser::findByLogin($login);
		//echo password_hash("a", PASSWORD_DEFAULT);
		//if ($pass == $u->pass) {
		if (password_verify ( $pass , $o->pass)){
			$this->user_login = $o->email;
			$this->logged_in = true;

			$_SESSION['user_login'] = $this->user_login;
			$_SESSION['access_level'] = $this->access_level;
		}
	}

	public function logout(){
		unset($_SESSION['user_login']);
		unset($_SESSION['user_level']);

		$this->user_login = null;
		$this->access_level = ACCESS_LEVEL_NONE;
		$this->logged_in = false;
	}

	public function checkAccessRight($requested){
		if($this->access_level >= $requested){
			return true;
		}
		return false;
	}

	public function addUser($login, $pass0, $pass1, $name){
		$o = Organiser::findByLogin($login);
		if ($pass0 != $pass1){
			return "Passwords différents";
		}
		if ($o != false){
			return "Login existant";
		}
		if ($login == "" or $pass0 == ""){
			return "Saisie obligatoire";
		}
		$o = new Organiser();
		$o->email = $login;
		$o->pass = password_hash($pass0, PASSWORD_DEFAULT);
		$o->name = $name;

		return $o->insert();
	}


}
