<?php
namespace sportnet\utils;

class ConnectionFactory{

	static private $config, $db;

	public static function setConfig($nom_fichier){
		self::$config = parse_ini_file($nom_fichier);
	}

	public static function makeConnection(){
		if (!isset(self::$db)){
			try {

			$dsn = "mysql:host=".self::$config['host'].";dbname=".self::$config['dbname'];
			$user = self::$config["user"];
			$pass = self::$config["pass"];

			self::$db = new \PDO($dsn, $user, $pass, array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

			}
			catch(\Exception $e){
				die("Erreur: " . $e->getMessage());
			}
		}
		return self::$db;
	}
}
