<?php
namespace sportnet\utils;

class Router extends AbstractRouter {

	public function addRoute($url, $ctrl, $mth, $level){
		self::$routes[$url] = array('ctrl' => $ctrl, 'mth' => $mth, 'level' => $level);
	}

	public function dispatch(HttpRequest $http_request){
		if (isset(self::$routes[$http_request->path_info])){
			$ctrl = self::$routes[$http_request->path_info]['ctrl'];
			$mth = self::$routes[$http_request->path_info]['mth'];
			$c = new $ctrl($http_request);
			$c->$mth();
		}
		else{
			$ctrl = self::$routes['default']['ctrl'];
			$mth = self::$routes['default']['mth'];
			$c = new $ctrl($http_request);
			$c->$mth();
		}
	}
}
