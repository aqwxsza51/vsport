<?php

namespace sportnet\utils;

abstract class AbstractRouter {

    /*
     * Attribut Statique qui stocke les routes possibles de l'application 
     * 
     * - Une route est reprÃ©sentÃ©e par un tableau :
     *       [ le controlleur, la methode, niveau requis ]
     * 
     * - Chaque route est stokÃ¨e dans le tableau $route sous la clÃ© qui est son
     *   URL (voir example en bas de ce fichier ) 
     * 
     */
    
    public static $routes = array ();

    /* 
     * MÃ©thode addRoute : ajoute une route a la liste des route 
     *
     * ParamÃ¨tres :
     *
     * - $url (String)  : l'url de la route
     * - $ctrl (String) : le nom de la classe du ContrÃ´leur 
     * - $mth (String)  : le nom de la mÃ©thode qui rÃ©alise la fonctionalitÃ© 
     *                     de la route
     * - $level (Integer) : le niveau d'accÃ¨s nÃ©cessaire pour la fonctionnalitÃ©
     * 
     * Algorythme :
     *
     * - Ajouter le tablau [ $ctrl, $mth, $level ] au tableau $this->route 
     *   sous la clÃ© $url
     *
     */
    
    abstract public function addRoute($url, $ctrl, $mth, $level);

    /*
     * MÃ©thode dispatch : execute une route en fonction de la requÃªte 
     *
     * ParamÃ¨tre :
     *  
     * - $http_request (HttpRequest) : Une instance de la classe HttpRequest
     *
     * Algorythme :
     *
     * - Si l'attribut $path_info existe dans $http_request
     *   ET si une route existe dans le tableau $route sous le nom $path_info
     *     - crÃ©er une instance du controleur de la route
     *     - exÃ©cuter la mÃ©thode de la route 
     * - sinon 
     *    - exÃ©cuter la route par dÃ©faut : 
     *        - crÃ©er une instance du controleur de la route par dÃ©fault
     *        - exÃ©cuter la mÃ©thode de la route par dÃ©fault
     * 
     */
    
    abstract public function dispatch(HttpRequest $http_request);

}
    
    /* 
       AprÃ¨s l'ajout de toutes les routes, le tableau $route ressemblera Ã  ceci :

Array
(
    [/wiki/all/] => Array
        (
            [0] => \sportnet\control\WikiController
            [1] => listAll
            [2] => -100
        )

    [/wiki/view/] => Array
        (
            [0] => \sportnet\control\WikiController
            [1] => viewPage
            [2] => -100
        )

    [default] => Array
        (
            [0] => \sportnet\control\WikiController
            [1] => listAll
            [2] => -100
        )

)
     */

