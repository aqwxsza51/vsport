<?php
namespace sportnet\utils;

class HttpRequest extends AbstractHttpRequest {

	public function __construct(){
		$this->script_name = $_SERVER['SCRIPT_NAME'];
		
		$this->path_info = $_SERVER['PATH_INFO'];
	
		$this->query = $_SERVER['QUERY_STRING'];
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->get = $_GET;
		$this->post = $_POST;
	}

	public function getRoot(){
		return dirname($this->script_name);
	}

	public function getController(){
		if (isset($this->path_info)) {
			return dirname($this->path_info);
		}
	}

	public function getAction(){
		if (isset($this->path_info)) {
			return basename($this->path_info);
		}
	}
}
