<?php

namespace sportnet\control;
use \sportnet\model\Participant as Participant ;
use \sportnet\model\Run as Run ;
use \sportnet\model\Participation as Participation ;
use \sportnet\model\Event as Event ;
use \sportnet\view\SNParticipationView as SNParticipationView;
use \sportnet\view\SNEventView as SNEventView;

class SNParticipationController {
	private $request=null;

	public function __construct(\sportnet\utils\HttpRequest $http_req){
	        $this->request = $http_req ;
	}




	public function consultInscription() {

		$data = Participant::findById($_POST['numParticipant']);
        $vue = new SNParticipationView($data);

		$vue->render('inscriptionParticipant');
	}

	public function inscriptionForm(){
		$idEvent = $_GET['id'];
		$event = Event::findById($idEvent);
		$vue = new SNParticipationView($event);
		$vue->render('inscriptionForm');

	}

	public function runRank() {

			$data = Event::findById($_GET['idEvent']);
			$vue = new SNParticipationView($data);
			$vue->render("runRank");


	}

	public function persoRank(){
		$data = Participant::findById($_POST['numParticipant']);
		$vue = new SNParticipationView($data);
		$vue->render('persoRank');

	}

	public function eventRank() {
		  $events = Event::findwithRank();
          $view = new SNParticipationView($events);
          $view->render('eventRank');

	}

	public function payer() {

		$participant = new Participant() ;
		$participant->firstName = $_POST['firstName'];
		$participant->lastName = $_POST['lastName'];
		$participant->email = $_POST['email'];
		$participant->birthDate = $_POST['birthDate'];
		$idParticipant = $participant->insert();

		for ($i = 1 ; $i <= $_POST['count'] ; $i++ ){
			$participation = new Participation();
			$participation->idParticipant = $idParticipant;
			$participation->idRun = $_POST['id'.$i.''];
			$participation->insert();
		}

			$v = new SNParticipationView($idParticipant);
	    $v->render("numParticipant");

	}

	public function recap() {


		$participant = new Participant() ;
		$participant->firstName = $_POST['firstName'];
		$participant->lastName = $_POST['lastName'];
		$participant->email = $_POST['email'];
		$participant->birthDate = $_POST['birth'];
		$runs = $_POST['run'];
		$data = [];
		$data[] = $participant;
		$data[] = $runs;
		$vue = new SNParticipationView($data);
		$vue->render('recap');

	}

	public function transfertRank(){
		if ($_POST['submit'] !== null and $_FILES['resultat']['error'] > 0) {
				echo "Erreur lors du transfert";
		}
		else {
				$flag = true;
				$handle = fopen($_FILES['resultat']['tmp_name'], "r");
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
						if ($flag) { $flag = false; continue; }

						$id = $_POST['id'];
						$idRun = $_POST['idRun'];
						$idParticipant = $data[0];
						$time = $data[4];
						$rank = $data[5];

						if (isset ($data[5])) {
								$p = new Participation;
								$p->updateRank($idRun, $idParticipant, $time, $rank);
						}
						else {
								break;
						}
				}

				fclose($handle);
		}

		$e = Event::FindById($id);
		$v = new SNEventView($e);
		$v->render("manageRun");

	}

	public function downloadParticipant(){

			$filename = "Épreuve-" . sprintf("%05d", $_POST['idRun']) . ".csv";

			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename="'. $filename .'";');

			$p = new Participation();
			$p->idRun = $_POST['idRun'];

			$fp = fopen('php://output',"w");

			$data[] = array("idParticipant","Nom","Prenom","Dossard","Temps","Rang");
			foreach($data as $field):
					fputcsv($fp, $field,";");
			endforeach;

			$data = $p->getRegistrationForAllParticipant();
			foreach($data as $field):
			    fputcsv($fp, $field,";");
			endforeach;

			fclose($fp);

	}

}
