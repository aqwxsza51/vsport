<?php

namespace sportnet\control;

use \sportnet\view\SNAdminView as SNAdminView;
use \sportnet\view\SNEventView as SNEventView;
use \sportnet\utils\Authentification as Authentification;
use \sportnet\model\Event as Event;
use \sportnet\model\Organiser as Organiser;

class SNAdminController {
    private $request=null;

    public function __construct(\sportnet\utils\HttpRequest $http_req){
        $this->request = $http_req ;
    }



    public function loginOrganiser() {
	        $v = new SNAdminView();
	        $v->render("login");
    }



    public function checkOrganiser(){
        $a = new Authentification();
        $email = filter_var($_POST['email'], FILTER_SANITIZE_SPECIAL_CHARS);
        $pass = $_POST['pass'];

        $a->login($email, $pass);
        if ($a->logged_in == true){

            $this->organiserSpace();
    	  }
    	  else {
        	  $v = new SNAdminView();
    		    $v->render("login");
    	  }

    }


    public function logoutOrganiser(){
        $a = new Authentification();
        $a->logout();
        $data = Event::findBecoming();
        $v = new SNEventView($data);
        $v->render("all");

     }


    public function organiserSpace(){
        $a = new Authentification();
        if ($a->logged_in == true){
            $o = Organiser::findBylogin($a->user_login);
            $data = $o->getEvents();
            $v = new SNEventView($data);
            $v->render("space");
        }
        else {
            $v = new SNAdminView();
            $v->render("login");
        }

    }

    public function createOrganiser() {
        $v = new SNAdminView();
        $v->render("create");
    }
}
