<?php
namespace sportnet\control;

use \sportnet\model\Event as Event;
use \sportnet\model\Organiser as Organiser;
use \sportnet\model\Run as Run;
use \sportnet\utils\Authentification as Authentification;
use \sportnet\view\SNEventView as SNEventView;

class SNEventController {


    private $request=null;


    public function __construct(\sportnet\utils\HttpRequest $http_req){
        $this->request = $http_req ;
    }


    public function listBecoming(){

          $data = Event::findBecoming();
          $v = new SNEventView($data);
	      $v->render("all");

    }

    public function organiserSpace(){

          $a = new Authentification();
          if ($a->logged_in == true){
            $o = Organiser::findByLogin($a->user_login);
            $data = $o->getEvents();
            $v = new SNEventView($data);
            $v->render("new");
          }

    }



    public function addOrganiser() {
        $a = new Authentification();
        $email = $_POST["email"];
        $pass0 = $_POST["pass0"];
        $pass1 = $_POST["pass1"];
        $name = $_POST["name"];

        $response = $a->addUser($email, $pass0, $pass1, $name);
        if (!is_String($response)){
            $data = "Utilisateur ajouté avec succès";
        } else {
          $data = $response;
        }
        $v = new SNEventView($data);
        $v->render("add");
    }

    public function validateEvent(){
        $a = new Authentification();
        $id = $_POST["id"];
        if ($a->logged_in == true){
          $e = Event::findById($id);
          $e->status = 'Événement validé';
          $e->updateStatus();
        }
        $v = new SNEventView($e);
        $v->render("manageRun");
    }

    public function openEvent(){
        $a = new Authentification();
        $id = $_POST["id"];
        if ($a->logged_in == true){
          $e = Event::findById($id);
          $e->status = 'Inscriptions ouvertes';
          $e->updateStatus();
        }
        $v = new SNEventView($e);
        $v->render("manageRun");
    }

    public function closeEvent(){
        $a = new Authentification();
        $id = $_POST["id"];
        if ($a->logged_in == true){
          $e = Event::findById($id);
          $e->status = 'Inscriptions fermées';
          $e->updateStatus();
        }
        $v = new SNEventView($e);
        $v->render("manageRun");
    }

    public function publishEvent(){
        $a = new Authentification();
        $id = $_POST["id"];
        if ($a->logged_in == true){
          $e = Event::findById($id);
          $e->status = 'Résultats publiés';
          $e->updateStatus();
        }
        $v = new SNEventView($e);
        $v->render("manageRun");
    }

    public function newEvent(){
      $a = new Authentification();
      if ($a->logged_in == true){
          $v = new SNEventView();
          $v->render("new");
      }
    }

    public function newRun(){
      $a = new Authentification();
      $id = $_POST["id"];
      $e = Event::FindById($id);
      if ($a->logged_in == true){
          $v = new SNEventView($e);
          $v->render("newRun");
      }
    }

    public function manageRun(){
      $a = new Authentification();
      $id = $_GET["id"];
      $e = Event::FindById($id);
      if ($a->logged_in == true){
          $v = new SNEventView($e);
          $v->render("manageRun");
      }
    }

    public function addEvent(){
      $a = new Authentification();
      $o = Organiser::findBylogin($a->user_login);
      if ($a->logged_in == true){
          $e = new Event();

          $e->label = $_POST['title'];
          $e->startDate = $_POST['date_start'];
          $e->endDate = $_POST['date_end'];
          $e->description = $_POST['description'];
          $e->status = "Événement créé";
          $e->website = $_POST['website'];
          $e->idDiscipline = $_POST['category'];
          $e->idOrga = $o->id;

          if (isset($_FILES['image']) && $_FILES['image'][error] < 1) {
          if ($_FILES['image']['type'] == "image/png") {
            $format= "png";
          }
          else {
            $format = "jpg";
          }
          $http = new \sportnet\utils\HttpRequest();
          $app_root = ($http->getRoot() == '/') ? "" : $http->getRoot();
          $random = md5(uniqid(rand(),true));
          $nom =  $_SERVER['DOCUMENT_ROOT'].$app_root."/assets/event-image/".$random.".".$format;
          $resultat = move_uploaded_file($_FILES['image']['tmp_name'], $nom);

          $e->photo = $random.".".$format;
        }
        else {
          $e->photo = "runners.jpg";
        }
          $e->place = $_POST['place'];

          if ($e->insert() != false){
            $data = $o->getEvents();
            $v = new SNEventView($data);
            $v->render("space");
          } else {
            $v = new SNEventView();
            $v->render("new");
          }
      }
    }

    public function addRun(){
      $a = new Authentification();
      $o = Organiser::findBylogin($a->user_login);
      if ($a->logged_in == true){
          $r = new Run();
  				$r->label = $_POST['title'];
  				$r->startTime = $_POST['time'];
  				$r->startDate = $_POST['date'];
  				$r->description = $_POST['description'];
  				$r->price = $_POST['price'];
  				$r->idEvent = $_POST['id'];

          $r->insert();
          $e = Event::FindById($_POST['id']);
          $v = new SNEventView($e);
          $v->render("manageRun");
      }
    }
}
