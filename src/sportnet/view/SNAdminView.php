<?php

namespace sportnet\view;

class SNAdminView  extends AbstractView{


    public function __construct($data = NULL){
        parent::__construct($data);
    }


    protected function renderLogin(){
      $form = <<<EOD
        <section class="col-12 row">
            <h1>Espace organisateur</h1>
            <article class="container">
          <h2 class="col-12">Connexion à l&#039;espace organisateur</h2>
            <form class="row col-6 col-md-12 col-sm-12" method="POST" action="$this->script_name/admin/validation/">
              <div class="col-6">
                <p class="col-12">E-mail :</p>
                <input type="email" name="email" class="col-12" required>
              </div>
              <div class="col-6">
                <p class="col-12">Mot de passe :</p>
                <input type="password" name="pass" class="col-12" required>
              </div>
              <input type="submit" name="submit" class="col-12" value="Se connecter">
            </form>
            <p class="col-12">Vous n&#039;avez pas de compte organisateur ?<br/>
            Créez-en un <a href="$this->script_name/admin/orga-inscription/">ici</a></p>
          </article>
      </section>
EOD;

return $form;
}


    protected function renderCreate(){
  $form = <<<EOD
    <section class="col-12 row">
    <h1>Espace organisateur</h1>
    <article class="container">
      <h2 class="col-12">Inscription à l&#039;espace organisateur</h2>
      <form class="row col-12 col-md-12 col-sm-12" method="POST" action="$this->script_name/admin/ajout/">
        <div class="col-6 col-sm-12">
          <p class="col-12">E-mail :</p>
          <input type="email" name="email" class="col-12" required>
        </div>
        <div class="col-6 col-sm-12">
          <p class="col-12">Nom d&#039;entreprise / association :</p>
          <input type="text" name="name" class="col-12" required>
        </div>
        <div class="col-6 col-sm-12">
          <p class="col-12">Choisissez un mot de passe :</p>
          <input type="password" name="pass0" class="col-12" required>
        </div>
        <div class="col-6 col-sm-12">
          <p class="col-12">Confirmez votre mot de passe :</p>
          <input type="password" name="pass1" class="col-12" required>
        </div>
        <input type="submit" name="submit" value="S'inscrire" class="col-12">
      </form>
    </article>
  </section>
EOD;
  return $form;
}


    public function render($selector){


        switch($selector){
        case 'login':
            $main = $this->renderLogin();
            break;
        case 'create':
            $main = $this->renderCreate();
            break;
        default:
            $main = $this->renderLogin();
            break;
        }

        $style_file = $this->app_root.'html/style.css';

        $header = $this->renderHeader();
        $menu   = $this->renderMenu();
        $footer = $this->renderFooter();

$html = <<<EOT
        <!DOCTYPE html>

        <html>
        <head>
          <meta charset="utf-8">
          <title>SportNet - Propulseur d événements sportifs</title>
          <link rel="stylesheet" type="text/css" href="$this->app_root/styles/frameworkCSS/css/main.css">
          <link rel="stylesheet" type="text/css" href="$this->app_root/styles/css/style.css">
        </head>
        <body>
          <header class="row">
                <a href="$this->app_root/index.php/">
              <img class="col-4 col-md-5 col-sm-8 off-sm-2" id="logo" src="$this->app_root/assets/img/logo.png" alt="Logo SportNet">
            </a>
                <nav class="row">
              ${menu}
                </nav>
          </header>
            ${main}
            <footer class="row col-12">
              <img class="col-sm-5" src="$this->app_root/assets/img/logo.png" alt="Logo SportNet">
              <p class="col-sm-7">&copy; 2016 - Les Placards Bell</p>
            </footer>
          </body>
          </html>
EOT;
    echo $html;

    }


}
