<?php

namespace sportnet\view;

use \sportnet\utils\Authentification as Authentification;


abstract class AbstractView {


    protected $app_root = null;    /* répertoire racine de l'application */
    protected $script_name = null; /* le chemin vers le script */
    protected $data = null ;       /* une page ou un tableau de page */

    /* Constructeur
    *
    * Prend en paramète une variable (un objet page ou un tableau de page)
    *
    * - Stock la variable dans l'attribut $data
    * - Recupérer la racine de l'application depuis un objet HttpRequest,
    *   pour construire les URL des liens  et des actions des formulaire
    *   et le nom du scripte pour les stocker and les attributs
    *   $app_root et $script_name
    *
    */
    public function __construct($data){
        $this->data = $data;

        $http = new \sportnet\utils\HttpRequest();
        $this->script_name  = $http->script_name;
        $this->app_root     = ($http->getRoot() == '/') ? "" : $http->getRoot();
    }

    public function __get($attr_name) {
        if (property_exists( $this, $attr_name))
            return $this->$attr_name;
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }

    public function __set($attr_name, $attr_val) {
        if (property_exists($this , $attr_name))
            $this->$attr_name=$attr_val;
        else{
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    public function __toString(){
        $prop = get_object_vars ($this);
        $str = "";
        foreach ($prop as $name => $val){
            if( !is_array($val) )
                $str .= "$name : $val <br> ";
            else
                $str .= "$name :". print_r($val, TRUE)."<br>";
        }
        return $str;
    }


    /*
     *  Crée le fragment HTML de l'entête
     *
     */
    protected function renderHeader(){
        $html = "";
        return $html;
    }

    /*
     * Crée le fragment HTML du bas de la page
     *
     */
    protected function renderFooter(){
        $html = "";
        return $html;
    }


    /*
     * Crée le fragment HTML dumenu
     *
     */

   protected function renderMenu(){
        $html =
            '<div class="row responsive-menu col-sm-12">
                <img src="'.$this->app_root.'/assets/img/menu.svg" alt="Menu">
                <p>MENU</p>
             </div>';

        $a = new Authentification();

        if ($a->logged_in == true){
            $html .=
                '<a class="col-2 col-sm-12" href="'.$this->app_root.'/index.php/participation/evenements/">Événements</a>
                 <a class="col-2 col-sm-12" href="'.$this->app_root.'/index.php/participation/classement/">Résultats</a>
                 <a class="col-2 col-sm-12" href="'.$this->app_root.'/index.php/admin/espace-orga/">Espace organisateur</a>
                 <a class="col-2 col-sm-12" href="'.$this->app_root.'/index.php/admin/orga-deconnection/">Déconnexion</a>';
        }
        else {
            $html .=
                '<a class="col-2 off-2 off-md-0 col-sm-12" href="'.$this->app_root.'/index.php/participation/evenements/">Événements</a>
                 <a class="col-2 col-sm-12" href="'.$this->app_root.'/index.php/participation/classement/">Résultats</a>
                 <a class="col-2 col-sm-12" href="'.$this->app_root.'/index.php/admin/orga-connection/">Espace organisateur</a>';
        }
        return $html;
    }



    /*
     * Affiche une page HTML complète.
     *
     * A definir dans les classe concrètes
     *
     */
    abstract public function render($selector);

}
