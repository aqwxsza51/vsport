<?php

namespace sportnet\view;

use \sportnet\model\Discipline as Discipline;

class SNEventView  extends AbstractView{


    public function __construct($data = NULL){
        parent::__construct($data);
    }


     protected function renderAll(){
        $html = '<section>
                <h1>Événements à venir</h1>';
        foreach ($this->data as $e){
             $html .= <<< EOD
            
               <a href="$this->app_root/index.php/participation/inscription/?id=$e->id">
               <article class="container row col-12">
                   <h2 class="col-12">$e->label</h2>
                   <div class="col-12">
                       <img class="col-4 col-md-6 col-sm-12" src="$this->app_root/assets/event-image/$e->photo" alt="runners">
                       <div class="col-3 col-md-6 col-sm-12">
    
                           <p>{$e->getDiscipline()->label}</p>
                           <p>$e->startDate - $e->endDate</p>
                           <p>$e->place</p>
                           <p>$e->status</p>
                       </div>
                       <div class="col-5 col-md-12 col-sm-12">
                           <div class="hidden-lg hidden-md col-sm-12">
                               <p class="align-center">Description</p>
                           </div>
                           <p class="col-sm-12">$e->description</p>
                       </div>
                   </div>
               </article>
               </a>
            
EOD;
        }
        return $html.'</section>';

    }


    protected function renderSpace(){
      $form = <<< EOD
      <section>
      <h1>Espace organisateur</h1>
        <article class="container row col-12">
            <form action="$this->script_name/evenement/nouveau/" method="post">
              <input type="submit" name="Aujouter un événement" class="col-12" value="Ajouter un événement">
            </form>
EOD;
      foreach ($this->data as $e){
          $form .= <<< EOD
          <div class="lighten-back col-12">
              <a href="$this->script_name/evenement/gerer-epreuve/?id=$e->id">
              <h2 class="col-12 event-title">$e->label</h2>
              <div class="col-12">
                  <img class="col-4 col-md-6 col-sm-12 img-event" src="$this->app_root/assets/event-image/$e->photo" alt="runners">
                  <div class="col-3 col-md-6 col-sm-12">
                      <p>{$e->getDiscipline()->label}</p>
                      <p>$e->startDate</p>
                      <p>$e->place</p>
                      <p>$e->status</p>
                  </div>
                  <div class="col-5 col-md-12 col-sm-12 event-descr">
                      <p>$e->description</p>
                  </div>
          </div>
            </a>
            </div>
EOD;
      }
      $form .= '</article>
            </section>';
      return $form;
    }

protected function renderManageRun(){
$html = <<< EOD
<section class="container col-12 ">
<h2 class="col-12">Titre de l&#039;événement</h2>
<article class="row">
    <img class="col-4 col-md-6 col-sm-12" src="$this->app_root/assets/event-image/{$this->data->photo}" alt="runners">
    <div class="event-details col-3 col-md-6 col-sm-12">
        <p>{$this->data->label}</p>
        <p>{$this->data->startDate} - {$this->data->endDate}</p>
        <p>{$this->data->place}</p>
        <p>{$this->data->status}</p>
        <p>{$this->data->countById()} participants</p>
        <p>Créé par {$this->data->getOrganiser()->name}</p>
        <p>{$this->data->website}</p>
    </div>
    <div class="col-5 col-md-12 col-sm-12 hidden-md hidden-sm">
EOD;

if ($this->data->status == "Événement créé") {
    $html .= '<form method="post" action="'. $this->script_name .'/evenement/validation/">
                        <input type="hidden" name="id" value="' . $this->data->id .'">
                        <input class="col-12 col-md-12 col-sm-12" value="Valider l&#039;événement" type="submit" name="valid">
              </form>';
}
if ($this->data->status == "Événement validé") {
    $html .= '<form method="post" action="'. $this->script_name .'/evenement/ouverture/">
                        <input type="hidden" name="id" value="' . $this->data->id .'">
                        <input class="col-12 col-md-12 col-sm-12" value="Ouvrir les inscriptions" type="submit" name="open-inscriptions">
              </form>';
}
if ($this->data->status == "Inscriptions ouvertes") {
    $html .= '<form method="post" action="'. $this->script_name .'/evenement/fermeture/">
                        <input type="hidden" name="id" value="' . $this->data->id .'">
                        <input class="col-12 col-md-12 col-sm-12" value="Fermer les inscriptions" type="submit" name="close-inscriptions">
              </form>';
}
if ($this->data->status == "Inscriptions fermées") {
    $html .= '<form method="post" action="'. $this->script_name .'/evenement/publication/">
                        <input type="hidden" name="id" value="' . $this->data->id .'">
                        <input class="col-12 col-md-12 col-sm-12" value="Publier les résultats" type="submit" name="publish-inscriptions">
              </form>';
}
  $html .= <<< EOD
  </div>
    <div class="responsive-run col-12">
        <h3 class="col-12">Description</h3>
        <p class="col-12">{$this->data->description}</p>
    </div>
    <form method="POST" action="$this->script_name/evenement/nouvelle-epreuve/">
        <input type="hidden" name="id" value="{$this->data->id}">
        <input type="submit" class="col-12 add-margin-sides" name="add_epreuve" value="Ajouter une nouvelle épreuve">
    </form>
    <div class="runs col-12">
        <h3 class="col-12">Épreuves</h3>
EOD;

      $run = $this->data->getRun();
      if (isset($run)){
        foreach($run as $r){
          $ref = sprintf("%05d", $r->id);
          $html .= <<< EOD
        <div class="lighten-back col-12">
            <div class="lighten-back responsive-run col-6 col-md-6 col-sm-12">
                <h4 class="col-12">$ref - $r->label </h4>
                <div class="col-12">
                    <p class="less-margin">$r->startDate à $r->startTime</p>
                    <p class="less-margin">{$r->countById()} participants</p>
                    <p class="less-margin">$r->price €</p>
                    <p class="less-margin"><br>$r->description</p>
                </div>
            </div>
            <div class="lighten-back col-6 col-md-6 col-sm-12">
                <form method="POST" action="$this->script_name/participation/liste-participants/">
                <input type="hidden" name="id" value="{$this->data->id}">
                <input type="hidden" name="idRun" value="$r->id">
                <input value="Télécharger la liste des participants" type="submit" name="download" class="col-12">
                </form>

                <form method="POST" action="$this->script_name/participation/transfert-resultats/" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{$this->data->id}">
                    <input type="hidden" name="idRun" value="$r->id">
                    <input type="file" name="resultat" class="col-8"/>
                    <input value="Transférer les résultats" type="submit" name="transfert" class="col-4">
                </form>
            </div>
        </div>
EOD;
      }
    }

      $html .= '</div>
</article>
</section>';

  return $html;
 }


 protected function renderNew(){
      $form = <<< EOD
    <section class="col-12 row">
      <h1>Espace organisateur</h1>
      <article class="container">
        <h2 class="col-12">Nouvel évènement :</h2>
        <form class="row col-12 col-md-12 col-sm-12" method="POST" action="$this->script_name/evenement/ajout/ "enctype="multipart/form-data">
          <div class="col-6 col-sm-12">
            <p class="col-6 col-md-12 col-sm-12">* Titre :</p>
            <input type="text" name="title" class="col-6 col-md-12 col-sm-12" required>
          </div>
          <div class="col-6 col-sm-12">
            <p class="col-6 col-md-12 col-sm-12">* Catégorie :</p>
            <select name="category" class="col-6 col-md-12 col-sm-12" required>
              <option value="">Choisissez une catégorie</option>
EOD;
  $discipline = Discipline::findAll();
  foreach($discipline as $d){
              $form .= '<option value="' . $d->id . '">' . $d->label  . '</option>';
  }
  $form .= <<< EOD
            </select>
          </div>
          <div class="col-6 col-sm-12">
            <p class="col-6 col-md-12 col-sm-12">* Date de début :</p>
            <input type="text" name="date_start" class="col-6 col-md-12 col-sm-12" placeholder="AAAA-MM-JJ" required>
          </div>
          <div class="col-6 col-sm-12">
            <p class="col-6 col-md-12 col-sm-12">* Date de fin :</p>
            <input type="text" name="date_end" class="col-6 col-md-12 col-sm-12" placeholder="AAAA-MM-JJ" required>
          </div>
          <div class="col-6 col-sm-12">
            <p class="col-6 col-md-12 col-sm-12">* Lieu :</p>
            <input type="text" name="place" class="col-6 col-md-12 col-sm-12" required>
          </div>
          <div class="col-6 col-sm-12">
            <p class="col-6 col-md-12 col-sm-12">Site de l&#039;événement :</p>
            <input type="url" name="website" class="col-6 col-md-12 col-sm-12" required>
          </div>
          <div class="row">
            <p class="col-12 col-md-12 col-sm-12">* Description :</p>
            <textarea name="description" class="col-12" rows="10" required></textarea>
          </div>
          <div class="row">
            <p class="col-12">Ajouter une image</p>
            <input type="file" name="image" class="col-12">
          </div>
          <input type="submit" name="submit" value="Créer cet événement" class="col-12">
        </form>
      </article>
EOD;
    return $form.'</section>';
    }

    protected function renderNewRun(){
      $form = <<< EOD
<section class="col-12 row">
  <h1>Espace organisateur</h1>
  <article class="container">
          <h2 class="col-12">Nouvelle épreuve pour l&#039;événement {$this->data->label}</h2>
    <form class="row col-12 col-md-12 col-sm-12" method="POST" action="$this->script_name/evenement/ajout-epreuve/">
      <div class="col-6 col-sm-12">
        <p class="col-6 col-md-12 col-sm-12">* Nom de l&#039;épreuve :</p>
        <input type="text" name="title" class="col-6 col-md-12 col-sm-12" required>
      </div>
      <div class="col-6 col-sm-12">
        <p class="col-6 col-md-12 col-sm-12">* Date :</p>
        <input type="text" name="date" class="col-6 col-md-12 col-sm-12" placeholder="AAAA-MM-JJ" required>
      </div>
              <div class="clearfix-lg"></div>
      <div class="col-6 col-sm-12">
        <p class="col-6 col-md-12 col-sm-12">* Heure de début :</p>
        <input type="text" name="time" class="col-6 col-md-12 col-sm-12" placeholder="00:00" required>
      </div>
      <div class="col-6 col-sm-12">
        <p class="col-6 col-md-12 col-sm-12">* Prix :</p>
        <input type="text" name="price" class="col-6 col-md-12 col-sm-12" placeholder="0" required>
      </div>
      <div class="row">
        <p class="col-12 col-md-12 col-sm-12">* Description :</p>
        <textarea name="description" class="col-12" rows="10" required></textarea>
      </div>
      <input type="submit" name="submit" value="Créer cette nouvelle épreuve" class="col-12">
      <input type="hidden" name="id" value="{$this->data->id}">
    </form>
  </article>
</section>
EOD;
      return $form;
    }


    public function renderAdd(){
        return '<section class="col-12">
        <h1>Organisateur</h1>
        <article class="container row col-12>"
          <p style="text-align:center; font-family: roboto-thin;">' . $this->data . '</p>
        <form method="post" action="'.$this->app_root.'/index.php/participation/evenements/">
            <input type="submit" class="col-4 col-sm-12 col-md-12 off-4 align-center" value="Revenir aux événements">
        </form></article> </section>';
    }

    /*
     * Affiche une page HTML complète.
     *
     * En focntion du sélécteur, le contenu de la page changera.
     *
     */
    public function render($selector){


        switch($selector){
        case 'space':
              $main = $this->renderSpace();
              break;
        case 'new':
            $main = $this->renderNew();
            break;
        case 'manageRun':
            $main = $this->renderManageRun();
            break;
        case 'newRun':
            $main = $this->renderNewRun();
            break;
        case 'all':
            $main = $this->renderAll();
            break;
        case 'add':
            $main = $this->renderAdd();
            break;
        default:
            $main = $this->renderAll();
            break;
        }

        $style_file = $this->app_root.'html/style.css';

        $header = $this->renderHeader();
        $menu   = $this->renderMenu();
        $footer = $this->renderFooter();

/*
 * Utilisation de la syntaxe HEREDOC pour ecrire la chaine de caractère de
 * la page entière. Voir la documentation ici:
 *
 * http://php.net/manual/fr/language.types.string.php#language.types.string.syntax.heredoc
 *
 * Noter bien l'utilisation des variable dans la chaine de caractère
 *
 */
$html = <<<EOT
        <!DOCTYPE html>
        <html>
        <head>

          <meta charset="utf-8">
          <title>SportNet - Propulseur d événements sportifs</title>
          <link rel="stylesheet" type="text/css" href="$this->app_root/styles/frameworkCSS/css/main.css">
          <link rel="stylesheet" type="text/css" href="$this->app_root/styles/css/style.css">
        </head>
        <body>
          <header class="row">
                <a href="$this->app_root/index.php/">
              <img class="col-4 col-md-5 col-sm-8 off-sm-2" id="logo" src="$this->app_root/assets/img/logo.png" alt="Logo SportNet">
            </a>
                <nav class="row">
              ${menu}
                </nav>
          </header>
           ${main}
            <footer class="row col-12">
              <img class="col-sm-5" src="$this->app_root/assets/img/logo.png" alt="Logo SportNet">
              <p class="col-sm-7">&copy; 2016 - Les Placards Bell</p>
            </footer>
          </body>
          </html>

EOT;

    echo $html;

    }


}
