<?php
namespace sportnet\view;
use \sportnet\model\Participation as Participation ;
use \sportnet\model\Participant as Participant ;
use \sportnet\model\Run as Run ;
class SNParticipationView extends AbstractView{
    /* Constructeur
    *
    * On appelle le constructeur de la classe parent
    *
    */
    public function __construct($data = NULL){
        parent::__construct($data);
    }
     protected function renderPartiInscri() {
      $participant = $this->data ;
      $participations = Participation::findByParticipant($participant->numParticipant);
      $html = '
    <section class="col-12 row">
        <h1>Consulter mon inscription</h1>
        <article class="container table-container">
          <p class="align-center col-12">Le participant n°'.$participant->numParticipant.' est inscrit aux épreuves suivantes :</p>
          <table class="col-12 align-center">
            <thead>
              <tr>
                <th>Épreuve</th>
                <th>Dossard n°</th>
                <th>Date</th>
                <th>Heure de Depart</th>
              </tr>
            </thead>';
            foreach($participations as $participation){

                        $run = Run::findByid($participation->idRun);

                             $html .= '<td>'.$run->label.'</td><td>'.$participation->bib.'</td><td>'.$run->startDate.'</td><td>'.$run->startTime.'</td></tr>';

                    }
                    $html .='</table>
                    <form method="post" action="'.$this->app_root.'/index.php/participation/evenements/">
                        <input type="submit" class="col-4 col-sm-12 col-md-12 off-4 align-center" value="Revenir aux événements">
                    </form>
                </article>
                </section>';
                return $html;
        return $html;
    }
    public function renderRunRank() {
        $html ="";
        $event = $this->data;
        $runs = $event->getRun();
        $html ='
        <section class="container col-12">
            <h2 class="col-12">'.$event->label.'</h2>
            <article class="row col-12">
                <img class="col-4 col-md-6 col-sm-12" src="'.$this->app_root.'/assets/event-image/'.$event->photo.'" alt="runners">
                <div class="event-details col-3 col-md-6 col-sm-12">
                    <p class="less-margin">'.$event->getDiscipline()->label.'</p>
                    <p class="less-margin">'.$event->startDate.' - '.$event->endDate.'</p>
                    <p class="less-margin">'.$event->place.'</p>
                    <p class="less-margin">'.$event->status.'</p>
                    <p class="less-margin">'.$event->getOrganiser()->name.'</p>
                    <p class="less-margin"><a href="'.$event->website.'">Site de l\'évenement</a></p>
                </div>
                <div class="col-5 col-md-12 col-sm-12">
                    <h4 class="col-12">Voir mes résultats :</h4>
                    <form action="'.$this->app_root.'/index.php/participation/classement-perso/" method="post">
                        <input class="col-8 col-md-12 col-sm-12" type="text" name="numParticipant" placeHolder="Numéro de participant" required>
                        <input class="col-4 col-md-12 col-sm-12" type="submit" value="Rechercher">
                    </form>
                </div>
                <div class="responsive-run col-12">
                    <h3 class="col-12">Description</h3>
                    <p class="col-12">'.$event->description.'</p>
                </div>
                <h3 class="col-12">Épreuves</h3>';
            foreach ($runs as $run) {
                $participations = $run->getParticipation();
            $html.= '
                <div class="lighten-back responsive-run col-12">
                    <h4 class="col-12">'.$run->label.'</h4>
                        <div class="col-12">
                            <p class="less-margin">Le '.$run->startDate.' à '.$run->startTime.'</p>
                            <p class="less-margin">'.$run->countById().' participants</p>
                            <p class="less-margin">'.$run->price.'</p>
                            <p class="less-margin"><br>'.$run->description.'</p>
                        </div>
                        <div class="responsive-run drop-down">
                            <h4 class="col-12">Résultats</h4>
                            <div class="col-12">
                                <div class="table-container col-12">
                                    <table class="col-12 align-center">
                                        <thead>
                                            <tr>
                                                <th>Rang</th>
                                                <th>Dossard</th>
                                                <th>Prénom</th>
                                                <th>Nom</th>
                                                <th>Temps</th>
                                            </tr>
                                        </thead>
                                        <tbody>';

                    foreach($participations as $participation){
                        if($participation->rank != NULL) {
                        $participant = Participant::findById($participation->idParticipant);
                                $html .= '<tr>
                                                <td>'.$participation->rank.'</td>
                                                <td>'.$participation->bib.'</td>
                                                <td>'.$participant->firstName.'</td>
                                                <td>'.$participant->lastName.'</td>
                                                <td>'.$participation->time.'</td>
                                            </tr>';
                        }
                    }
                            $html.= '</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>';
            }
            $html .= '
        </article>
    </section>';
    return $html;
    }
    protected function renderPersoRank() {
            $participant = $this->data ;
            if($participant != NULL){
            $participations = Participation::findByParticipant($participant->numParticipant);
              $html = '
            <section class="col-12 row">
                <h1>Consulter mes résultats</h1>
                <article class="container table-container">
                    <p class="align-center col-12">Resultats du parcitipant n°'.$participant->numParticipant.' :</p>
                    <table class="col-12 align-center">
                        <thead>
                            <tr>
                                <th>Epreuve</th>
                                <th>Classement</th>
                                <th>Temps</th>
                                <th>Dossard n°</th>
                            </tr>
                        </thead>';
                foreach($participations as $participation){

                    $run = Run::findByid($participation->idRun);
                    if ($participation->rank != NULL){
                         $html .= '<tr><td>'.$run->label.'</td><td>'.$participation->rank.'</td><td>'.$participation->time.'</td><td>'.$participation->bib.'</td></tr>';
                    }
                }
                $html .='</table>';
             } else {
                $html .= '<div>Participant inexistant</div>';
             }
              $html .= '
                    <form method="post" action="'.$this->app_root.'/index.php/participation/classement/">
                        <input type="submit" class="col-4 col-sm-12 col-md-12 off-4 align-center" value="Revenir aux événements">
                    </form>
            </article>
          </section>';
            return $html;
    }
    protected function renderEventRank(){
        $events = $this->data;
        $html = "";
        $html .='<section class="row">
        <h1>Résultats</h1>';
        foreach($events as $event){
            $html .='<a href="'.$this->app_root.'/index.php/participation/classement-epreuve/?idEvent='.$event->id.'">
            <article class="col-4 col-md-12 col-sm-12 container results">
                <h2 class="col-12">'.$event->label.'</h2>
                <img src="'.$this->app_root.'/assets/event-image/'.$event->photo.'" alt="resultat" class="col-12 col-md-6 col-sm-12">
                <div class="col-md-6 col-sm-12">
                    <p>'.$event->getDiscipline()->label.'</p>
                    <p>'.$event->startDate.' - '.$event->endDate.'</p>
                    <p>'.$event->place.'</p>
                    <p>'.$event->status.'</p>
                </div>
            </article>
        </a>';
        }
        $html .= '</section>';
        return $html ;
    }
   protected function renderRecap(){
      $data = $this->data ;
      $total = 0;
        $participant = $data[0];
        $idrun = $data[1];
        $count = 0;

     $html = '<section class="col-12 row">
            <h1>Confirmer mon inscription</h1>
            <article class="container">
            <form method="post" action="'.$this->app_root.'/index.php/participation/payer/">';
      // FORM
      $html .='<span style="display: none;"><input type="text" name="lastName" value="'.$participant->lastName.'" readonly><br/>
       <input type="text" name="firstName" value="'.$participant->firstName.'" readonly><br/>
       <input type="email" name="email" value="'.$participant->email.'" readonly>
       <input type="text" name="birthDate" value="'.$participant->birthDate.'" readonly></span>';
        $html .= '
                <p class="align-center"><span style="font-family: roboto">'.$participant->firstName.'</span>, veuillez payer afin de confirmer votre inscription aux épreuves suivantes :</p>';
       // END FORM
        foreach($idrun as $run){
            $runObj = Run::findById($run);
            $count++;
            $html .='<p class="align-center">'.$runObj->label.'</p>';
            // FORM
            $html .= '<span style="display: none;"><input type="text" name="label" value="'.$runObj->label.'" readonly>
            <input type="text" name="price" value="'.$runObj->price.'" readonly>€
            <input type="text" name="id'.$count.'" value="'.$runObj->id.'" readonly></span>';
            $total = $total + $runObj->price;
            // END FORM

        }
        $html .= '<span style="display: none;"><input type="text" name="count" value="'.$count.'" readonly></span>';
        $html .= '<input class="col-12 col-md-12 col-sm-12" type="submit" value="Payer '.$total.' €">
        </form>
            </article>
        </section>';
        return $html;
    }
    protected function renderInscriptionForm() {
      $event = $this->data;
      $orga = $event->getOrganiser();
      $disci = $event->getDiscipline();
      $runs = $event->getRun();

      $html = '
            <section class="container col-12">
                <h2 class="col-12">'.$event->label.'</h2>
                <article class="container row col-12">
                        <img class="col-4 col-md-6 col-sm-12" src="'.$this->app_root.'/assets/event-image/'.$event->photo.'" alt="runners">
                        <div class="event-details col-3 col-md-6 col-sm-12">
                            <p class="less-margin">'.$event->getDiscipline()->label.'</p>
                            <p class="less-margin">'.$event->startDate.' - '.$event->endDate.'</p>
                            <p class="less-margin">'.$event->place.'</p>
                            <p class="less-margin">'.$event->status.'</p>
                            <p class="less-margin">Créé par '.$orga->name.'</p>
                            <a href="'.$event->website.'" class="less-margin">Site de l\'événement</a>
                        </div>
                        <div class="col-5 col-md-12 col-sm-12">
                            <h4 class="col-12">Consulter mon inscription :</h4>
                            <form action="'.$this->app_root.'/index.php/participation/consult-inscription/" method="post">
                                <input class="col-8 col-md-12 col-sm-12" type="text" name="numParticipant" placeHolder="Numéro de participant" required>
                                <input class="col-4 col-md-12 col-sm-12" type="submit" value="Rechercher">
                            </form>
                        </div>
                        <div class="responsive-run col-12">
                            <h3 class="col-12">Description</h3>
                            <p class="col-12">'.$event->description.'</p>
                        </div>
                        <div class="runs col-12">
                        <h3 class="col-12">Épreuves</h3>';
      if(isset($runs)){
        foreach ($runs as $run) {
            $html .= '
                    <div class="lighten-back responsive-run col-6 col-md-12 col-sm-12">
                        <h4 class="col-12">'.$run->label.'</h4>
                        <div class="col-12">
                            <p class="less-margin">Le '.$run->startDate.' à '.$run->startTime.'</p>
                            <p class="less-margin">'.$run->countById().' participants</p>
                            <p class="less-margin">Prix : '.$run->price.'€</p>
                            <p class="less-margin"><br>'.$run->description.'</p>
                        </div>
                    </div>';
        }
      }

        if($event->status == "Inscriptions ouvertes") {
            $html .= '
                </div>
                    <div class="add-margin-sides lighten-back col-12 row">
                        <h3 class="col-12">Inscriptions</h3>
                        <form method="post" action="'.$this->app_root.'/index.php/participation/recap/">
                            <div class="col-6 col-sm-12">
                                <p class="col-4 col-md-12 col-sm-12">Prénom :</p>
                                <input class="col-8 col-md-12 col-sm-12" type="text" name="lastName" required>
                            </div>
                            <div class="col-6 col-sm-12">
                                <p class="col-4 col-md-12 col-sm-12">Nom :</p>
                                <input class="col-8 col-md-12 col-sm-12" type="text" name="firstName" required>
                            </div>
                            <div class="col-6 col-sm-12">
                                <p class="col-4 col-md-12 col-sm-12">E-mail :</p>
                                <input class="col-8 col-md-12 col-sm-12" type="email" name="email" required>
                            </div>
                            <div class="col-6 col-sm-12">
                                <p class="col-4 col-md-12 col-sm-12">Date de naissance :</p>
                                <input class="col-8 col-md-12 col-sm-12" type="text" name="birth" placeHolder="AAAA-MM-JJ">
                            </div>
                            <div class="col-12">
                             <h4 class="col-12">S&#039;inscrire à :</h4>';

            foreach($runs as $run) {
                $html .= '
                        <div class="col-4 col-md-6 col-sm-12">
                            <label class="col-11" for="'.$run->id.'"><input class="col-1" type="checkbox" name="run[]" id="'.$run->id.'" value="'.$run->id.'">'.$run->label.'</label>
                        </div>';
            }
            $html .= '
                    </div>
                    <input class="col-12" type="submit" value="Je m&#039;inscris">
                    </form>
                </div>';
            }
        $html.='</article>
            </section>';
        return $html;
}

    public function renderNumParticipant(){
        return '<section class="col-12">
        <h1>Participation</h1>
        <article class="container row col-12>"
          <p style="text-align:center; font-family: roboto-thin;"> Voici votre numéro de participant :' . $this->data . '</p>
        <form method="post" action="'.$this->app_root.'/index.php/participation/evenements/">
            <input type="submit" class="col-4 col-sm-12 col-md-12 off-4 align-center" value="Revenir aux événements">
        </form></article> </section>';
    }

    /*
     * Affiche une page HTML complète.
     *
     * En focntion du sélécteur, le contenu de la page changera.
     *
     */
    public function render($selector){
        switch($selector){
        case 'inscriptionParticipant':
            $main = $this->renderPartiInscri();
            break;
        case 'view':
            $main = $this->renderView();
            break;
        case 'inscriptionForm' :
            $main = $this->renderInscriptionForm();
            break;
        case 'recap' :
            $main = $this->renderRecap();
            break;
        case 'eventRank' :
            $main = $this->renderEventRank();
            break;
        case 'runRank' :
            $main = $this->renderRunRank();
            break;
        case 'persoRank' :
            $main = $this->renderPersoRank();
            break;
        case 'all':
            $main = $this->renderAll();
            break;
        case 'add':
            $main = $this->renderAdd();
            break;
        case 'numParticipant' :
            $main = $this->renderNumParticipant();
            break;
        default:
            $main = $this->renderAll();
            break;
        }
        $style_file = $this->app_root.'html/style.css';
        $header = $this->renderHeader();
        $menu   = $this->renderMenu();
        $footer = $this->renderFooter();


$html = <<<EOT
        <!DOCTYPE html>
        <html>
        <head>
          <meta charset="utf-8">
          <title>SportNet - Propulseur d événements sportifs</title>
          <link rel="stylesheet" type="text/css" href="$this->app_root/styles/frameworkCSS/css/main.css">
          <link rel="stylesheet" type="text/css" href="$this->app_root/styles/css/style.css">
        </head>
        <body>
          <header class="row">
                <a href="$this->app_root/index.php/">
              <img class="col-4 col-md-5 col-sm-8 off-sm-2" id="logo" src="$this->app_root/assets/img/logo.png" alt="Logo SportNet">
            </a>
                <nav class="row">
              ${menu}
                </nav>
          </header>
           ${main}
            <footer class="row col-12">
              <img class="col-sm-5" src="$this->app_root/assets/img/logo.png" alt="Logo SportNet">
              <p class="col-sm-7">&copy; 2016 - Les Placards Bell</p>
            </footer>
          </body>
          </html>
EOT;
    echo $html;
    }
}
