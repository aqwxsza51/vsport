<?php require_once ('header.php'); ?>
    <section class="container col-12">
        <h2 class="col-12">Titre de l'événement</h2>
        <article class="row col-12">
            <img class="col-4 col-md-6 col-sm-12" src="../assets/img/runners.jpg" alt="runners">
            <div class="event-details col-3 col-md-6 col-sm-12">
                <p class="less-margin">Marathon</p>
                <p class="less-margin">12/12/2016 - 14/12/2016</p>
                <p class="less-margin">Vandoeuvre-lès-Nancy</p>
                <p class="less-margin">Résultats publiés</p>
                <p class="less-margin">503 participants</p>
                <p class="less-margin">Créé par Run'n'co</p>
                <p class="less-margin">Site de l'événement</p>
            </div>
            <div class="col-5 col-md-12 col-sm-12">
                <h4 class="col-12">Voir mes résultats :</h4>
                <form action="#" method="post">
                    <input class="col-8 col-md-12 col-sm-12" type="text" name="numParticipant" placeHolder="Numéro de participant" required>
                    <input class="col-4 col-md-12 col-sm-12" type="submit" value="Rechercher">
                </form>
            </div>
            <div class="responsive-run col-12">
                <h3 class="col-12">Description</h3>
                <p class="col-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat. Quisque faucibus luctus sagittis. Vestibulum ut rhoncus urna, eu rutrum massa. Maecenas cursus, enim iaculis porta pulvinar, nisl augue porta nisi, nec pellentesque nisi neque ut nulla. Curabitur viverra ex eu euismod suscipit. In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Vivamus arcu libero, varius non est a, maximus suscipit velit. Vivamus in dignissim ipsum, ac accumsan enim.In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Vivamus arcu libero, varius non est a, maximus suscipit velit. Vivamus in dignissim ipsum, ac accumsan enim.</p>
            </div>
            <div class="runs col-12">
                <h3 class="col-12">Épreuves</h3>
                <div class="lighten-back responsive-run col-12">
                    <h4 class="col-12">Nom de l'épreuve</h4>
                    <div class="col-12">
                        <p class="less-margin">12/12/2016 à 09h30</p>
                        <p class="less-margin">504 participants</p>
                        <p class="less-margin">30,00€</p>
                        <p class="less-margin"><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat. Quisque faucibus luctus sagittis. Vestibulum ut rhoncus urna, eu rutrum massa. Maecenas cursus, enim iaculis porta pulvinar, nisl augue porta nisi, nec pellentesque nisi neque ut nulla. Curabitur viverra ex eu euismod suscipit. In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat.</p>
                    </div>
                    <div class="responsive-run drop-down">
                        <h4 class="col-12">Résultats</h4>
                        <div class="col-12">
                            <div class="table-container col-12">
                                <table class="col-12 align-center">
                                    <thead>
                                        <tr>
                                            <th>Rang</th>
                                            <th>Dossard</th>
                                            <th>Prénom</th>
                                            <th>Nom</th>
                                            <th>Temps</th>
                                        </tr>
                                    </thead>
                    
                                    <tbody>
                                        <tr>
                                            <td>{rank}</td>
                                            <td>{bib}</td>
                                            <td>{firstName}</td>
                                            <td>{name}</td>
                                            <td>{time}</td>
                                        </tr>
                                        <!-- A supprimer -->
                                        <tr>
                                            <td>{rank}</td>
                                            <td>{bib}</td>
                                            <td>{firstName}</td>
                                            <td>{name}</td>
                                            <td>{time}</td>
                                        </tr>
                                        <tr>
                                           <td>{rank}</td>
                                            <td>{bib}</td>
                                            <td>{firstName}</td>
                                            <td>{name}</td>
                                            <td>{time}</td>
                                        </tr>
                                        <!-- Fin suppression pour vue -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
<?php require_once ('footer.php'); ?>