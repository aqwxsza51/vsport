<?php require_once ('header.php'); ?>
    <section class="container col-12 ">
        <h2 class="col-12">Titre de l'événement</h2>
        <article class="row"> 
            <img class="col-4 col-md-6 col-sm-12" src="../assets/img/runners.jpg" alt="runners">
            <div class="event-details col-3 col-md-6 col-sm-12">
                <p>Marathon</p>
                <p>12/12/2016 - 14/12/2016</p>
                <p>Vandoeuvre-lès-Nancy</p>
                <p>Inscriptions ouvertes</p>
                <p>503 participants</p>
                <p>Créé par Run'n'co</p>
                <p>Site de l'événement</p>
            </div>
            <div class="col-5 col-md-12 col-sm-12 hidden-md hidden-sm">
                <form action="#">
                    <input class="col-12 col-md-12 col-sm-12" type="submit" name="valid_event" value="Valider l'événement">
                </form>
                <form action="#">
                    <input class="col-12 col-md-12 col-sm-12" type="submit" name="open_inscriptions" value="Ouvrir les inscriptions">
                </form>
            </div>
            <div class="responsive-run col-12">
                <h3 class="col-12">Description</h3>
                <p class="col-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat. Quisque faucibus luctus sagittis. Vestibulum ut rhoncus urna, eu rutrum massa. Maecenas cursus, enim iaculis porta pulvinar, nisl augue porta nisi, nec pellentesque nisi neque ut nulla. Curabitur viverra ex eu euismod suscipit. In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Vivamus arcu libero, varius non est a, maximus suscipit velit. Vivamus in dignissim ipsum, ac accumsan enim.In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Vivamus arcu libero, varius non est a, maximus suscipit velit. Vivamus in dignissim ipsum, ac accumsan enim.</p>
            </div>
            <form method="POST" action="orga-nouvelle-epreuve.php">
                <input type="submit" class="col-12 add-margin-sides" name="add_epreuve" value="Ajouter une nouvelle épreuve">
            </form>
            <div class="runs col-12">
                <h3 class="col-12">Épreuves</h3>
                <!-- DEBUT A DUPLIQUER -->
                <div class="lighten-back col-12">
                    <div class="lighten-back responsive-run col-6 col-md-6 col-sm-12">
                        <h4 class="col-12">Nom de l'épreuve</h4>
                        <div class="col-12">
                            <p class="less-margin">12/12/2016 à 09h30</p>
                            <p class="less-margin">504 participants</p>
                            <p class="less-margin">30,00€</p>
                            <p class="less-margin"><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat. Quisque faucibus luctus sagittis. Vestibulum ut rhoncus urna, eu rutrum massa. Maecenas cursus, enim iaculis porta pulvinar, nisl augue porta nisi, nec pellentesque nisi neque ut nulla. Curabitur viverra ex eu euismod suscipit. In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat.</p>
                        </div>
                    </div>
                    <div class="lighten-back col-6 col-md-6 col-sm-12">
                        <form method="POST" action="#">
                            <input value="Télécharger la liste des participants" type="submit" name="download" class="col-12">
                        </form>
                        <form method="POST" action="#">
                            <input value="Transférer les résultats" type="submit" name="transfert" class="col-12">
                        </form>
                        <form method="POST" action="#">
                            <input value="Publier les résultats" type="submit" name="publish" class="col-12">
                        </form>
                    </div>
                </div>
                <!-- FIN DE DUPLICATION -->
            </div>
        </article>
    </section>
<?php require_once ('footer.php'); ?>