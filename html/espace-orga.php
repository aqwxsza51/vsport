<?php require_once("header.php"); ?>
	<section>
		<h1>Espace organisateur</h1>
        <article class="container row col-12">
        	<form action="orga-nouvel-evenement.php" method="post">
        		<input type="submit" name="Aujouter un événement" class="col-12" value="Ajouter un événement">
        	</form>
            <!-- DEBUT DUPLICATION -->
            <h2 class="col-12 event-title">Titre événement 1</h2>
            <div class="col-12 lighten-back">
                <img class="col-4 col-md-6 col-sm-12 img-event" src="../assets/img/runners.jpg" alt="runners">
                <div class="col-3 col-md-6 col-sm-12">
                    <p>Marathon</p>
                    <p>12/12/2016</p>
                    <p>Vandoeuvre-lès-Nancy</p>
                    <p>Inscriptions ouvertes</p>
                </div>
	 			<div class="responsive-run col-5 col-md-12 col-sm-12">
	 				<h4 class="col-12 hidden-lg hidden-md dark">Gérer l'événement</h4>
	 				<div class="col-12">
		 				<form method="post" action="#">
		                    <input value="Valider l'événement" type="submit" name="valid" class="col-12">
		                </form>
		                <form method="post" action="#">
		                    <input value="Ouvrir les inscriptions" type="submit" name="open-inscriptions" class="col-12">
		                </form>
		                <form method="post" action="#">
		                    <input value="Gérer les épreuves" type="submit" name="manage" class="col-12">
		                </form>
	                </div>
	 			</div>
 			</div>
 			<!-- FIN DUPLICATION -->
		</article>
	</section>
<?php require_once("footer.php"); ?>