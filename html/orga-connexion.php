<?php require_once("header.php"); ?>
	<section class="col-12 row">
        <h1>Espace organisateur</h1>
		<article class="container">
			<h2 class="col-12">Connexion à l'espace organisateur</h2>
				<form class="row col-6 col-md-12 col-sm-12" method="POST" action="#">
					<div class="col-6">
						<p class="col-12">E-mail :</p>
						<input type="email" name="email" class="col-12" required>
					</div>
					<div class="col-6">
						<p class="col-12">Mot de passe :</p>
						<input type="password" name="password" class="col-12" required>
					</div>
					<input type="submit" name="submit" class="col-12" value="Se connecter">
				</form>
				<p class="col-12">Vous n'avez pas de compte organisateur ?<br/>
				Créez-en un <a href="orga-inscription.php">ici</a></p>
			</article>
	</section>
<?php require_once("footer.php"); ?>