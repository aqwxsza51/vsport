<?php require_once("header.php"); ?>

<?php require_once("header.php"); ?>
	<section class="col-12 row">
		<h1>Espace organisateur</h1>
		<article class="container">
			<h2 class="col-12">Nouvel évènement :</h2>
			<form class="row col-12 col-md-12 col-sm-12" method="POST" action="#">
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Titre :</p>
					<input type="text" name="title" class="col-6 col-md-12 col-sm-12" required>
				</div>
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Catégorie :</p>
					<select name="category" class="col-6 col-md-12 col-sm-12" required>
						<option value="">Choisissez une catégorie</option>
						<option value="{label-category}">{category}</option>
						<option value="{label-category}">{category}</option>						
					</select>
				</div>
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Date de début :</p>
					<input type="text" name="date_start" class="col-6 col-md-12 col-sm-12" placeholder="JJ/MM/AAAA" required>
				</div>
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Date de fin :</p>
					<input type="text" name="date_end" class="col-6 col-md-12 col-sm-12" placeholder="JJ/MM/AAAA" required>
				</div>
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Lieu :</p>
					<input type="text" name="place" class="col-6 col-md-12 col-sm-12" required>
				</div>
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">Site de l'événement :</p>
					<input type="url" name="website" class="col-6 col-md-12 col-sm-12" required>
				</div>
				<div class="row">
					<p class="col-12 col-md-12 col-sm-12">* Description :</p>
					<textarea name="description" class="col-12" rows="10" required></textarea>
				</div>
				<div class="row">
					<p class="col-12">Ajouter une image</p>
					<input type="file" name="image" class="col-12">
				</div>
				<input type="submit" name="submit" value="Créer cet événement" class="col-12">
			</form>
		</article>
	</section>
<?php require_once("footer.php"); ?>