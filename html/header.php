<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>SportNet - Propulseur d'événements sportifs</title>
	<link rel="stylesheet" type="text/css" href="../styles/frameworkcSS/css/main.css">
	<link rel="stylesheet" type="text/css" href="../styles/css/style.css">
</head>
<body>
	<header class="row">
		<a href="index.php">
			<img class="col-4 col-md-5 col-sm-8 off-sm-2" id="logo" src="../assets/img/logo.png" alt="Logo SportNet">
		</a>
		<nav class="row">
			<div class="row responsive-menu col-sm-12">
				<img class="" src="../assets/img/menu.svg" alt="Menu">
				<p class="">MENU</p>
			</div>
            <a class="col-2 off-2 off-md-0 col-sm-12" href="index.php">Événements</a>
            <a class="col-2 col-sm-12" href="resultats.php">Résultats</a>
            <a class="col-2 col-sm-12" href="orga-connexion.php">Espace organisateur</a>
        </nav>	
	</header>