<?php require_once("header.php"); ?>
	<section class="col-12 row">
        <h1>Espace organisateur</h1>
		<article class="container">
            <h2 class="col-12">Nouvelle épreuve:</h2>
			<form class="row col-12 col-md-12 col-sm-12" method="POST" action="#">
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Nom de l'épreuve :</p>
					<input type="text" name="name_epreuve" class="col-6 col-md-12 col-sm-12" required>
				</div>
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Date :</p>
					<input type="text" name="date" class="col-6 col-md-12 col-sm-12" placeholder="JJ/MM/AAAA" required>
				</div>
                <div class="clearfix-lg"></div>
				<div class="col-6 col-sm-12">
					<p class="col-6 col-md-12 col-sm-12">* Heure de début :</p>
					<input type="text" name="hour" class="col-6 col-md-12 col-sm-12" placeholder="00h00" required>
				</div>
				<div class="row">
					<p class="col-12 col-md-12 col-sm-12">* Description :</p>
					<textarea name="description" class="col-12" rows="10" required></textarea>
				</div>
				<input type="submit" name="submit" value="Créer cette nouvelle épreuve" class="col-12">
			</form>
		</article>
	</section>
<?php require_once("footer.php"); ?>