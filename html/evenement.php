<?php require_once ('header.php'); ?>
    <section class="container col-12">
        <h2 class="col-12">Titre de l'événement</h2>
        <article class="container row col-12">
            <img class="col-4 col-md-6 col-sm-12" src="../assets/img/runners.jpg" alt="runners">
            <div class="event-details col-3 col-md-6 col-sm-12">
                <p class="less-margin">Marathon</p>
                <p class="less-margin">12/12/2016 - 14/12/2016</p>
                <p class="less-margin">Vandoeuvre-lès-Nancy</p>
                <p class="less-margin">Inscriptions ouvertes</p>
                <p class="less-margin">503 participants</p>
                <p class="less-margin">Créé par Run'n'co</p>
                <p class="less-margin">Site de l'événement</p>
            </div>
            <div class="col-5 col-md-12 col-sm-12">
                <h4 class="col-12">Consulter mon inscription :</h4>
                <form action="#" method="post">
                    <input class="col-8 col-md-12 col-sm-12" type="text" name="numParticipant" placeHolder="Numéro de participant" required>
                    <input class="col-4 col-md-12 col-sm-12" type="submit" value="Rechercher">
                </form>
            </div>
            <div class="responsive-run col-12">
                <h3 class="col-12">Description</h3>
                <p class="col-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat. Quisque faucibus luctus sagittis. Vestibulum ut rhoncus urna, eu rutrum massa. Maecenas cursus, enim iaculis porta pulvinar, nisl augue porta nisi, nec pellentesque nisi neque ut nulla. Curabitur viverra ex eu euismod suscipit. In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Vivamus arcu libero, varius non est a, maximus suscipit velit. Vivamus in dignissim ipsum, ac accumsan enim.In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Vivamus arcu libero, varius non est a, maximus suscipit velit. Vivamus in dignissim ipsum, ac accumsan enim.</p>
            </div>
            <div class="runs col-12">
                <h3 class="col-12">Épreuves</h3>
                <div class="lighten-back responsive-run col-6 col-md-12 col-sm-12">
                    <h4 class="col-12">Nom de l'épreuve</h4>
                    <div class="col-12">
                        <p class="less-margin">12/12/2016 à 09h30</p>
                        <p class="less-margin">504 participants</p>
                        <p class="less-margin">30,00€</p>
                        <p class="less-margin"><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat. Quisque faucibus luctus sagittis. Vestibulum ut rhoncus urna, eu rutrum massa. Maecenas cursus, enim iaculis porta pulvinar, nisl augue porta nisi, nec pellentesque nisi neque ut nulla. Curabitur viverra ex eu euismod suscipit. In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat.</p>
                    </div>
                </div>
            </div>
            <div class="add-margin-sides lighten-back col-12 row">
                <h3 class="col-12">Inscriptions</h3>
                <form method="post" action="#">
                    <div class="col-6 col-sm-12">
                        <p class="col-4 col-md-12 col-sm-12">Prénom :</p>
                        <input class="col-8 col-md-12 col-sm-12" type="text" name="firstName" required>
                    </div>
                    <div class="col-6 col-sm-12">
                        <p class="col-4 col-md-12 col-sm-12">Nom :</p>
                        <input class="col-8 col-md-12 col-sm-12" type="text" name="name" required>
                    </div>
                    <div class="col-6 col-sm-12">
                        <p class="col-4 col-md-12 col-sm-12">E-mail :</p>
                        <input class="col-8 col-md-12 col-sm-12" type="email" name="email" required>
                    </div>
                    <div class="col-6 col-sm-12">
                        <p class="col-4 col-md-12 col-sm-12">Date de naissance :</p>
                        <input class="col-8 col-md-12 col-sm-12" type="text" name="birth" placeHolder="JJ/MM/AAAA">
                    </div>
                    <div class="col-12">
                        <h4 class="col-12">S'inscrire à :</h4>
                        <div class="col-4 col-md-6 col-sm-12">
                            <label class="col-11" for="e1"><input class="col-1" type="checkbox" name="run[]" id="e1" value="e1">Nom de l'épreuve 1</label>
                        </div>
                        <div class="col-4 col-md-6 col-sm-12">
                            <label class="col-11" for="e2"><input class="col-1" type="checkbox" name="run[]" id="e2">Nom de l'épreuve 2</label>
                        </div>
                        <div class="col-4 col-md-6 col-sm-12">
                            <label class="col-11" for="e3"><input class="col-1" type="checkbox" name="run[]" id="e3">Nom de l'épreuve 3</label>
                        </div>
                      
                    </div>
                    <input class="col-12" type="submit" value="Je m'inscris">
                </form>
            </div>
        </article>
    </section>
<?php require_once ('footer.php'); ?>
