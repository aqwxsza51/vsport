<?php require_once ('header.php'); ?>
    <section>
        <h1>Événements à venir</h1>
        <a href="evenement.php">
            <article class="container row col-12">
                <h2 class="col-12 event-title">Titre événement 1</h2>
                <div class="col-12">
                    <img class="col-4 col-md-6 col-sm-12 img-event" src="../assets/img/runners.jpg" alt="runners">
                    <div class="col-3 col-md-6 col-sm-12">
                        <p>Marathon</p>
                        <p>12/12/2016</p>
                        <p>Vandoeuvre-lès-Nancy</p>
                        <p>Inscriptions ouvertes</p>
                    </div>
                    <div class="col-5 col-md-12 col-sm-12 event-descr">
                        <div class="hidden-lg hidden-md col-sm-12">
                            <p class="align-center">Description</p>
                        </div>
                        <p class="col-sm-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur ante eu cursus volutpat. Quisque faucibus luctus sagittis. Vestibulum ut rhoncus urna, eu rutrum massa. Maecenas cursus, enim iaculis porta pulvinar, nisl augue porta nisi, nec pellentesque nisi neque ut nulla. Curabitur viverra ex eu euismod suscipit. In blandit consectetur libero vel tristique. Praesent molestie a mauris in rhoncus. Fusce rhoncus accumsan turpis in ullamcorper. Vivamus arcu libero, varius non est a, maximus suscipit velit. Vivamus in dignissim ipsum, ac accumsan enim.</p>
                    </div>
                </div>
            </article>
        </a>
    </section>
<?php require_once ('footer.php'); ?>