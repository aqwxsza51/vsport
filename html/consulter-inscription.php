<?php require_once("header.php"); ?>
	<h1>Consulter mon inscription</h1>
	<section class="container col-12 row">
		<article class="table-container">
			<p class="align-center col-12">Le participant n°{numParticipant} est inscrit aux épreuves suivantes :</p>
			<table class="col-12 align-center">
				<thead>
					<tr>
						<th>Epreuve</th>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Dossard n°</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>{epreuve}</td>
						<td>{nom}</td>
						<td>{prenom}</td>
						<td>{bib}</td>
					</tr>
					<!-- A supprimer -->
					<tr>
						<td>{epreuve}</td>
						<td>{nom}</td>
						<td>{prenom}</td>
						<td>{bib}</td>
					</tr>
					<tr>
						<td>{epreuve}</td>
						<td>{nom}</td>
						<td>{prenom}</td>
						<td>{bib}</td>
					</tr>
					<!-- Fin suppression pour vue -->
				</tbody>
			</table>
			<form method="post" action="#">
				<input type="submit" class="col-4 col-sm-12 col-md-12 off-4 align-center" value="Revenir sur la page de l'événement">
			</form>
		</article>
	</section>
<?php require_once("footer.php"); ?>