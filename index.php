<?php
require_once("conf/autoload.php");
use \sportnet\utils\ConnectionFactory as ConnectionFactory ;

use \sportnet\model\Organiser as Organiser ;
use \sportnet\model\Participant as Participant ;
use \sportnet\model\Run as Run ;

use \sportnet\control\SNParticipationController as SNParticipationController;
use \sportnet\utils\HttpRequest as HttpRequest;
use \sportnet\utils\Router as Router;

define("ACCESS_LEVEL_NONE",  -100);

session_start();

ConnectionFactory::setConfig("conf/config.ini");

$router = new Router();

$router->addRoute('/participation/consult-inscription/',  '\sportnet\control\SNParticipationController', 'consultInscription',ACCESS_LEVEL_NONE);

//Administration
$router->addRoute('/admin/orga-connection/', '\sportnet\control\SNAdminController', 'loginOrganiser',  ACCESS_LEVEL_NONE);
$router->addRoute('/admin/validation/','\sportnet\control\SNAdminController', 'checkOrganiser',  ACCESS_LEVEL_NONE);
$router->addRoute('/admin/espace-orga/', '\sportnet\control\SNAdminController', 'OrganiserSpace',  ACCESS_LEVEL_NONE);
$router->addRoute('/admin/orga-inscription/', '\sportnet\control\SNAdminController', 'createOrganiser',  ACCESS_LEVEL_NONE);
$router->addRoute('/admin/ajout/','\sportnet\control\SNEventController', 'addOrganiser', ACCESS_LEVEL_NONE);
$router->addRoute('/admin/orga-deconnection/', '\sportnet\control\SNAdminController', 'logoutOrganiser',  ACCESS_LEVEL_NONE);


//Événements
$router->addRoute('/evenement/liste/', '\sportnet\control\SNEventController', 'listBecoming',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/nouveau/', '\sportnet\control\SNEventController', 'newEvent',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/ajout/', '\sportnet\control\SNEventController', 'addEvent',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/validation/', '\sportnet\control\SNEventController', 'validateEvent',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/ouverture/', '\sportnet\control\SNEventController', 'openEvent',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/fermeture/', '\sportnet\control\SNEventController', 'closeEvent',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/publication/', '\sportnet\control\SNEventController', 'publishEvent',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/gerer-epreuve/', '\sportnet\control\SNEventController', 'manageRun',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/nouvelle-epreuve/', '\sportnet\control\SNEventController', 'newRun',  ACCESS_LEVEL_NONE);
$router->addRoute('/evenement/ajout-epreuve/', '\sportnet\control\SNEventController', 'addRun',  ACCESS_LEVEL_NONE);


//Participation
$router->addRoute('/participation/inscription/', '\sportnet\control\SNParticipationController', 'inscriptionForm',  ACCESS_LEVEL_NONE);
$router->addRoute('/participation/recap/', '\sportnet\control\SNParticipationController', 'recap',  ACCESS_LEVEL_NONE);
$router->addRoute('/participation/payer/', '\sportnet\control\SNParticipationController', 'payer',  ACCESS_LEVEL_NONE);
$router->addRoute('/participation/classement/', '\sportnet\control\SNParticipationController', 'eventRank',  ACCESS_LEVEL_NONE);
$router->addRoute('/participation/classement-epreuve/', '\sportnet\control\SNParticipationController', 'runRank',  ACCESS_LEVEL_NONE);
$router->addRoute('/participation/classement-perso/', '\sportnet\control\SNParticipationController', 'persoRank',ACCESS_LEVEL_NONE);
$router->addRoute('/participation/evenements/', '\sportnet\control\SNEventController', 'listBecoming',  ACCESS_LEVEL_NONE);
$router->addRoute('/participation/liste-participants/', '\sportnet\control\SNParticipationController', 'downloadParticipant',  ACCESS_LEVEL_NONE);
$router->addRoute('/participation/transfert-resultats/', '\sportnet\control\SNParticipationController', 'transfertRank',  ACCESS_LEVEL_NONE);


//Default
$router->addRoute('default', '\sportnet\control\SNEventController', 'listBecoming',  ACCESS_LEVEL_NONE);


$http_req = new HttpRequest();
$router->dispatch($http_req);
